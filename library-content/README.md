Intents-Content
===============

This module contains group of builders that may be used to build and start an intents that target 
**content** like **Image**, **Video**, ..., related applications.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aintents/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aintents/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:intents-content:${DESIRED_VERSION}@aar"

_depends on:_
[intents-core](https://bitbucket.org/android-universum/intents/src/main/library-core),
[intents-mimetype](https://bitbucket.org/android-universum/intents/src/main/library-mimetype)
    
## Components ##

Below are listed some of **primary components** that are available in this module:

- [ImageIntent](https://bitbucket.org/android-universum/intents/src/main/library-content/src/main/java/universum/studios/android/intent/ImageIntent.java)
- [VideoIntent](https://bitbucket.org/android-universum/intents/src/main/library-content/src/main/java/universum/studios/android/intent/VideoIntent.java)
- [ShareIntent](https://bitbucket.org/android-universum/intents/src/main/library-content/src/main/java/universum/studios/android/intent/ShareIntent.java)