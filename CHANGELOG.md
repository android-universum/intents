Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.2.2](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 25.04.2020

- Regular **maintenance**.

### [1.2.1](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.2.0](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 15.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.1.0](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 15.06.2018

- Small updates and improvements.

### [1.0.5](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 27.11.2017

- Removed `components` and `methods` **deprecated** in previous versions.
- Updated **dependencies** versions and _Gradle_ configuration.

### [1.0.4](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 09.08.2017

- **Deprecated** `packageName` attribute of `PlayIntent` and added **better named** alternative `applicationId`.

### [1.0.3](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 29.04.2017

- Removed **deprecated** `IntentsConfig` class.

### [1.0.2](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 19.04.2017

- Default type of `CalendarIntent` has been changed from `CalendarIntent.TYPE_INSERT_EVENT` to 
  `CalendarIntent.TYPE_VIEW` so `new CalendarIntent().startWith(...)` will simply open a **Calendar**
  application.

### [1.0.1](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 04.03.2017

- Code quality improvements.

### [1.0.0](https://bitbucket.org/android-universum/intents/wiki/version/1.x) ###
> 15.12.2016

- First production release.