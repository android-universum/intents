Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/intents/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/intents/src/main/library-core)**
- **[Calendar](https://bitbucket.org/android-universum/intents/src/main/library-calendar)**
- **[Contact](https://bitbucket.org/android-universum/intents/src/main/library-contact)**
- **[Content](https://bitbucket.org/android-universum/intents/src/main/library-content)**
- **[Map](https://bitbucket.org/android-universum/intents/src/main/library-map)**
- **[MimeType](https://bitbucket.org/android-universum/intents/src/main/library-mimetype)**
- **[Play](https://bitbucket.org/android-universum/intents/src/main/library-play)**
- **[Web](https://bitbucket.org/android-universum/intents/src/main/library-web)**
