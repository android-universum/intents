Intents-Web
===============

This module contains a builder that may be used to build and start an intent that targets **web**
related applications.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aintents/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aintents/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:intents-web:${DESIRED_VERSION}@aar"

_depends on:_
[intents-core](https://bitbucket.org/android-universum/intents/src/main/library-core)

## Components ##

Below are listed some of **primary components** that are available in this module:

- [WebIntent](https://bitbucket.org/android-universum/intents/src/main/library-web/src/main/java/universum/studios/android/intent/WebIntent.java)